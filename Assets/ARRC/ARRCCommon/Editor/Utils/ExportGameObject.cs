﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;
using ARRC.Commons.Coordinates;

public class ExportGameObject : EditorWindow
{
    //14178593.27, 4351594
    //(14177360.0, 48.4, 4351766.0)
    //public Vector3 offset_global = new Vector3(14177975.966f+1233.27f‬, 49.329f, 4351687.006f-172f);
    //public Vector3 offset_global = new Vector3(0‬, 0, 0);
    public Vector3 offset_global = new Vector3(14177975.996f + 1233.27f, 49.329f, 4351687.006f-1172);
    public List<GameObject> targets;

    Vector2 scrollPos = Vector2.zero;

    private void OnGUI()
    {
        scrollPos = EditorGUILayout.BeginScrollView(scrollPos);
        
        //EditorGUILayout.BeginVertical(GUI.skin.box);
        ScriptableObject target = this;
        SerializedObject serializedObject = new SerializedObject(target);
        
        EditorGUILayout.PropertyField(serializedObject.FindProperty("offset_global"), true);
        EditorGUILayout.PropertyField(serializedObject.FindProperty("targets"), true); // True means show children
        serializedObject.ApplyModifiedProperties(); // Remember to apply modified properties
        serializedObject.Update();
        //EditorGUILayout.EndVertical();

        GUILayout.EndScrollView();

        if (GUILayout.Button("Export"))
        {
            string lastPath = EditorPrefs.GetString("a4_OBJExport_lastPath", "");
            string lastFileName = EditorPrefs.GetString("a4_OBJExport_lastFile", "unityexport.obj");
            string expFile = EditorUtility.SaveFilePanel("Export OBJ", lastPath, lastFileName, "obj");
            
            Vector3 position = GetCenterPosition(targets);
            //GameObject dummy = GameObject.CreatePrimitive(PrimitiveType.Cylinder);
            //dummy.transform.position = position;
            //dummy.transform.localScale = new Vector3(30, 30, 30);
            //Debug.Log(position);

            //Write gps
            //Vector3 googleMercator = offset_global + position;
            //Debug.Log(googleMercator);

            //var wgs84 = WGS84Converter.Instance.GetLatLon(googleMercator.x, googleMercator.z);
            //Debug.Log(wgs84);
            
            OBJExporter objExporter = new OBJExporter();
            if (expFile.Length > 0)
            {
                //Write obj.
                var fi = new System.IO.FileInfo(expFile);
                EditorPrefs.SetString("a4_OBJExport_lastFile", fi.Name);
                EditorPrefs.SetString("a4_OBJExport_lastPath", fi.Directory.FullName);
                objExporter.Export(expFile, position, false);
            }

        }

    }

    private void OnEnable()
    {
        OnSelectionChange();
        WGS84Converter.Instance.Init("EPSG:3857");
    }

    Vector3 GetCenterPosition(List<GameObject> list_go)
    {
        var verts_aa = list_go.Select(item => item.GetComponent<MeshFilter>().sharedMesh.vertices);
        var vertices_all = verts_aa.SelectMany(item => item).Distinct();

        //vertices_all.OrderBy(item => Vector3.Distance(Vector3.zero, item));
        //Vector3 center_mid = vertices_all.ToList()[vertices_all.Count() / 2];

        Vector3 center = Vector3.zero;
        foreach(var pos in vertices_all)
        {
            center += pos;
        }
        center /= vertices_all.Count();

        Matrix4x4 mat_local2World = list_go[0].transform.localToWorldMatrix;
        Matrix4x4 mat_in_local = Matrix4x4.Translate(new Vector4(center.x, center.y, center.z, 1));
        Matrix4x4 mat_in_world = mat_local2World * mat_in_local;

        Vector4 trans = mat_in_world.GetColumn(3);

        return new Vector3(trans.x, trans.y, trans.z);

    }
    void OnSelectionChange()
    {
        var filteredList = new List<GameObject>();

        if (Selection.gameObjects.Length > 0)
        {
            foreach (GameObject candidate in Selection.gameObjects)
            {
                filteredList.Add(candidate);
            }
            targets = filteredList;

            Repaint();
        }
    }
    private void OnDestroy()
    {
        WGS84Converter.Instance.Dispose();
    }
    [MenuItem("Tools/Export Gameobject")]
    public static void OpenWindow()
    {
        
        GetWindow<ExportGameObject>(false, "Export");
    }
}
