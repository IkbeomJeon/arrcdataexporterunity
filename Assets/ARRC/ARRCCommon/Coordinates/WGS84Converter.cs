﻿using System;
using UnityEngine;

namespace ARRC.Commons.Coordinates
{
    public class WGS84Converter
    {
        Terrain terrain;
        CoordinateInfo coordinateInfo_toWGS;
        CoordinateInfo CoordinateInfo_fromWGS;

        public string sourceCRSCode
        {
            get; private set;
        }

        public void Init(string sourceCRSCode, Terrain terrain = null)
        {
            this.sourceCRSCode = sourceCRSCode;
            coordinateInfo_toWGS = new CoordinateInfo();
            coordinateInfo_toWGS.InitConverter(sourceCRSCode, "EPSG:4326");

            CoordinateInfo_fromWGS = new CoordinateInfo();
            CoordinateInfo_fromWGS.InitConverter("EPSG:4326", sourceCRSCode);
        }

        public (double lat, double lon) GetLatLon(double x, double z)
        {
            coordinateInfo_toWGS.ConvertXY(x, z, out double _lon, out double _lat);

            var output = (_lat, _lon);
            
            return output;
        }

        public (float x, float z) GetXZ(double lat, double lon)
        {
            CoordinateInfo_fromWGS.ConvertXY(lon, lat, out double _x, out double _z);

            var output = ((float)_x, (float)_z);
            return output;
        }

        public Vector3 GetXYZ(double lat, double lon)
        {
            if(terrain == null)
                throw new NullReferenceException("Reference terrin is not exsit");

            var WorldXZ = GetXZ(lat, lon);

            float y = terrain.SampleHeight(new Vector3(WorldXZ.x, 0, WorldXZ.z));

            return new Vector3(WorldXZ.x, y, WorldXZ.z);
        }
        public void Dispose()
        {
            instance = null;
        }

        static WGS84Converter instance;
        //public static WGS84Converter Instance
        //{
        //    get
        //    {
        //        if (instance == null)
        //        {
        //            GameObject obj = GameObject.Find("WGS84Converter");

        //            if (obj == null)
        //            {
        //                obj = new GameObject("WGS84Converter");
        //                instance = obj.AddComponent<WGS84Converter>();
        //            }
        //            else
        //            {
        //                instance = obj.GetComponent<WGS84Converter>();
        //            }
        //            DontDestroyOnLoad(obj);
        //        }
        //        return instance;
        //    }
        //}
        public static WGS84Converter Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new WGS84Converter();
                }

                return instance;
            }
        }

    }
}
