﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ARRC.Commons;

namespace ARRC.Commons.Coordinates
{
    public class CoordinateDB : ARRCXMLItem
    {
        
        private static Dictionary<string, string> CRSDictionary = new Dictionary<string, string>()
        {
            { "WGS84",              "EPSG:4326"}, // GPS가 사용하는 좌표계
            { "Google Mercator",    "EPSG:3857"}, // 구글지도/빙지도/야후지도/OSM 등 에서 사용중인 좌표계
            { "UTMK",               "EPSG:5179"}, // 네이버지도에서 사용중인 좌표계
            { "EPSG:4979",          "EPSG:4979"}, // same to wgs 84
            { "EPSG:5179",          "EPSG:5179"},
            { "EPSG:5175",          "EPSG:5175"},
            { "EPSG:25833",          "EPSG:25833"}, //berlin
            { "EPSG:31256",          "EPSG:31256"} //vin

            
            // GPS가 사용하는 좌표계
        };
        public static Dictionary<string, string> CRSParameters = new Dictionary<string, string>()
        {
            { "EPSG:4979",          "+proj=longlat +ellps=WGS84 +datum=WGS84 +no_defs  "},
            { "EPSG:5179",          "+proj=tmerc +lat_0=38 +lon_0=127.5 +k=0.9996 +x_0=1000000 +y_0=2000000 +ellps=GRS80 +units=m +no_defs"},
            { "EPSG:5175",          "+proj=tmerc +lat_0=38 +lon_0=127.00289 +k=1 +x_0=200000 +y_0=550000 +ellps=bessel +units=m +no_defs" },
            //{ "EPSG:31256",        "+proj=tmerc +lat_0=0 +lon_0=16.33333333333333 +k=1 +x_0=0 +y_0=-5000000 +ellps=bessel +towgs84=577.326,90.129,463.919,5.137,1.474,5.297,2.4232 +units=m +no_defs"}
        };

        public static string[] CRSKeys
        {
            get
            {
                var keys = new string[CRSDictionary.Keys.Count];
                CRSDictionary.Keys.CopyTo(keys, 0);
                return keys;
            }
        }
        public static string GetCRSCode(string nickname)
        {
            return CRSDictionary[nickname];
        }
        public static string GetCRSCode(int index)
        {
            return CRSDictionary.ElementAt(index).Value;
        }

        public static int GetCRSIndex(string code)
        {
            if(!CRSDictionary.ContainsValue(code))
                CRSDictionary.Add(code, code);

            return CRSDictionary.Values.ToList().IndexOf(code);
        }
    }
}


