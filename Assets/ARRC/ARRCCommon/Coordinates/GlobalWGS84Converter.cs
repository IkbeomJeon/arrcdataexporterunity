﻿using System;
using UnityEngine;

namespace ARRC.Commons.Coordinates
{
    public class GlobalWGS84Converter : MonoBehaviour
    {
        Terrain terrain;
        CoordinateInfo coordinateInfo_toWGS;
        CoordinateInfo CoordinateInfo_fromWGS;

        public string sourceCRSCode
        {
            get; private set;
        }

        public void Init(string sourceCRSCode, Terrain terrain)
        {
            this.sourceCRSCode = sourceCRSCode;
            coordinateInfo_toWGS = new CoordinateInfo();
            coordinateInfo_toWGS.InitConverter(sourceCRSCode, "EPSG:4326");

            CoordinateInfo_fromWGS = new CoordinateInfo();
            CoordinateInfo_fromWGS.InitConverter("EPSG:4326", sourceCRSCode);

            this.terrain = terrain;
        }

        public (double lat, double lon) GetLatLon(double x, double z)
        {
            coordinateInfo_toWGS.ConvertXY(x, z, out double _lon, out double _lat);

            var output = (_lat, _lon);
            
            return output;
        }
        public (double lat, double lon) GetLatLon(Vector3 position)
        {
            coordinateInfo_toWGS.ConvertXY(position.x, position.z, out double _lon, out double _lat);

            var output = (_lat, _lon);

            return output;
        }
        public (float x, float z) GetXZ(double lat, double lon)
        {
            CoordinateInfo_fromWGS.ConvertXY(lon, lat, out double _x, out double _z);

            var output = ((float)_x, (float)_z);
            return output;
        }

        public Vector3 GetXYZ(double lat, double lon)
        {
            if(terrain == null)
                throw new NullReferenceException("Reference terrin is not exsit");

            var WorldXZ = GetXZ(lat, lon);

            float y = terrain.SampleHeight(new Vector3(WorldXZ.x, 0, WorldXZ.z));

            return new Vector3(WorldXZ.x, y, WorldXZ.z);
        }
        public float GetHeight(Vector3 position)
        {
            return terrain.SampleHeight(position);
        }

        static GlobalWGS84Converter instance;
        public static GlobalWGS84Converter Instance
        {
            get
            {
                if (instance == null)
                {
                    GameObject obj = GameObject.Find("GlobalWGS84Converter");

                    if (obj == null)
                    {
                        obj = new GameObject("GlobalWGS84Converter");
                        instance = obj.AddComponent<GlobalWGS84Converter>();
                    }
                    else
                    {
                        instance = obj.GetComponent<GlobalWGS84Converter>();
                    }
                    DontDestroyOnLoad(obj);
                }
                return instance;
            }
        }


    }
}
